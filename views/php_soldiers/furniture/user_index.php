<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniPro' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php'); //using absolute path
//include_once('vendor/autoload.php');
//include_once('../../startup.php');

use APP\BITM\PHP_Soldiers\FURNITURE\User\User;
use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

//Utility::prx($_POST);
if (!isset($_SESSION['username']) or !isset($_SESSION['password'])) {
    header("location: login.php");
    $_message = "Please login";
    Utility::message($_message);
}


$User = new User();
$users = $User->index();
?>
<?php include 'layout/header.php'; ?>

<!-- start: Content -->
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="sub_category_index.php">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Tables</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Sub Category Panel</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <h3><?php echo Utility::message() ?></h3>
            <div class="box-content">

                <table class="table table-striped table-bordered bootstrap-datatable datatable">

                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Gender</th>
                            <th>Password</th>
                            <th>Status</th>
                            <th>Active</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $num = 1;
                        foreach ($users as $user) {
                            ?>
                            <tr>
                                <td><?php echo $num; ?></td>
                                <td><?php echo $user->first_name; ?></td>
                                <td><?php echo $user->last_name; ?></td>
                                <td><?php echo $user->email; ?></td>
                                <td><?php echo $user->mobile; ?></td>
                                <td><?php echo $user->gender; ?></td>
                                <td><?php echo $user->password; ?></td>
                                <?php
                                if ($user->deletion_status == 0) {
                                    ?>
                                    <td class="center">
                                        <span class="label label-success">Active</span>
                                    </td>
                                    <?php
                                } else {
                                    ?>
                                    <td class="center">
                                        <span class="label label-warning">Banned</span>
                                    </td>
                                    <?php
                                }
                                ?>
                                <td class="center">
                                    <a class="btn btn-success" title="View" href="user_view.php?user_id=<?php echo $user->user_id ?>">
                                        <i class="halflings-icon white zoom-in"></i>  
                                    </a>
                                    <a class="btn btn-info" title="Edit" href="user_edit.php?user_id=<?php echo $user->user_id ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                    <a class="btn btn-danger" class="delete" title="Delete"  href="user_delete.php?user_id=<?php echo $user->user_id ?>" onclick="return check();">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                    <?php
                                    if ($user->deletion_status == 0) {
                                        ?>
                                        <a style="width: 60px; height: 30px; background-color: green; color: white; padding: 8px;" title="Trash" href="sub_category_trash.php?status=unpublished&sub_category_id=<?php echo $user->sub_category_id ?>">Trash</a>
                                        <?php
                                    } else {
                                        ?>
                                        <a style="width: 50px; height: 50px; background-color: red; color: white; padding: 8px;" title="Recover" href="sub_category_trash.php?status=published&sub_category_id=<?php echo $user->sub_category_id ?>">Recover</a>
                                        <?php
                                    }
                                    ?>

                                </td>
                            </tr>
                            <?php
                            $num++;
                        }
                        ?>

                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->
    <a class="btn btn-success" href="account.php">Add Account</a><br>

</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<script type="text/javascript" src="js/jquery.js"></script>
<script>
                                    function check() {
                                        var chk = confirm("Are you sure to Delete?");
                                        if (chk) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    }
</script>

<?php include 'layout/footer.php'; ?>