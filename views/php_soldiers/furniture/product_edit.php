<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniPro' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php'); //using absolute path
//include_once('vendor/autoload.php');
//include_once('../../startup.php');

use APP\BITM\PHP_Soldiers\FURNITURE\Category\Category;
use APP\BITM\PHP_Soldiers\FURNITURE\Sub_Category\Sub_Category;
use APP\BITM\PHP_Soldiers\FURNITURE\Product\Product;
use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

if (!isset($_SESSION['username']) or !isset($_SESSION['password'])) {
    header("location: login.php");
    $_message = "Please login";
    Utility::message($_message);
}

$category = new Category();
$categorys = $category->select_all_published_category();
//Utility::prx($categorys);

$sub_category = new Sub_Category();
$sub_categorys = $sub_category->select_all_published_sub_category();
//Utility::prx($categorys);
$product = new Product();
$products = $product->get_single_id_details($_GET['product_id']);
//Utility::prx($products);
?>

<?php include 'layout/header.php'; ?>

<!-- start: Content -->
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="sub_category_index.php">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Forms</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>User Edit</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form action="product_update.php" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="title">Product Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused"
                                       autofocus="autofocus" 
                                       id="title" 
                                       type="text" 
                                       name="product_name"
                                       value="<?php echo $products->product_name; ?>"
                                       tabindex="1"
                                       placeholder="input product name"
                                       required="required" >
                            </div>
                        </div>
                        <div class="control-group">
                           <div class="controls">
                               <input type="hidden" 
                                       name="product_id"
                                       value="<?php echo $products->product_id; ?>"
                                       >
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="selectError3">Category Name</label>
                            <div class="controls">
                                <select id="selectError3" name="category_id">
                                    <option value="">Select Category Name</option>
                                    <?php foreach ($categorys as $category) { ?>
                                        <option value="<?php echo $category->category_id; ?>"><?php echo $category->category_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="selectError3">Sub Category Name</label>
                            <div class="controls">
                                <select id="selectError3" name="sub_category_id">
                                    <option value="">Select Sub Category Name</option>
                                    <?php foreach ($sub_categorys as $sub_category) { ?>
                                        <option value="<?php echo $sub_category->sub_category_id; ?>"><?php echo $sub_category->sub_category_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="title">Product Code</label>
                            <div class="controls">
                                <input class="input-xlarge focused"
                                       autofocus="autofocus" 
                                       id="title" 
                                       type="text" 
                                       name="product_code"
                                       value="<?php echo $products->product_code; ?>"
                                       tabindex="4"
                                       placeholder="input product code"
                                       required="required" >
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="title">Product Price</label>
                            <div class="controls">
                                <input class="input-xlarge focused"
                                       autofocus="autofocus" 
                                       id="title" 
                                       type="text" 
                                       name="product_price"
                                       value="<?php echo $products->product_price; ?>"
                                       tabindex="5"
                                       placeholder="input product price"
                                       required="required" >
                            </div>
                        </div>

                        <div class="control-group hidden-phone">
                            <label class="control-label" for="textarea2">Description</label>
                            <div class="controls">
                                <textarea 
                                    class="cleditor" 
                                    id="textarea2" 
                                    rows="3"
                                    type="text" 
                                    name="description"
                                    tabindex="6"
                                    required="required"
                                    > <?php echo $products->description; ?>
                                </textarea>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="fileInput">Image</label>
                            <div class="controls">
                                <input class="input-file uniform_on" 
                                       id="fileInput" 
                                       type="file"
                                       tabindex="7"
                                       name="imgName"
                                       >
                            </div><br>
                            <img style="margin-left: 180px;" src="<?php echo $products->image_1; ?>" alt="HTML5 Icon" width="270" height="230">
                        </div>
                        <div class="form-actions">
                            <button type="submit" tabindex="8" class="btn btn-primary">Save</button>
                            <input tabindex="9" class="btn" type="reset" value="Reset" />
                        </div>
                    </fieldset>
                </form>    
            </div>
        </div><!--/span-->
        <a class="btn btn-success" href="index.php">Go to List</a>
        <a class="btn btn-success" href="javascript:history.go(-1)">Back</a>

    </div><!--/row-->






</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<?php include 'layout/footer.php'; ?>