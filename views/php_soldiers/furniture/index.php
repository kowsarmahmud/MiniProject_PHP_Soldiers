
<?php //include 'front_end_layout/header.php';  ?>
<?php include 'navmanu.php'; ?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniPro' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php'); //using absolute path
//include_once('vendor/autoload.php');
//include_once('../../startup.php');

use APP\BITM\PHP_Soldiers\FURNITURE\Product\Product;
//use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

//Utility::prx($_GET);

$product = new Product();
$products = $product->select_all_published_product();
//Utility::prx($products);
?>

<div class="content">
    <div class="container">
        <div class="slider">
            <ul class="rslides" id="slider1">
                <li><img src="../../../Resource/front_end_asset/images/banner1.jpg" alt=""></li>
                <li><img src="../../../Resource/front_end_asset/images/banner2.jpg" alt=""></li>
                <li><img src="../../../Resource/front_end_asset/images/banner3.jpg" alt=""></li>
            </ul>
        </div>
    </div>
</div>
<!---->
<div class="bottom_content">
    <div class="container">
        <div class="sofas">
            <div class="col-md-6 sofa-grid">
                <img src="../../../Resource/front_end_asset/images/t1.jpg" alt=""/>
                <h3>IMPORTED DINING SETS</h3>
                <h4><a href="products.php">SPECIAL ACCENTS OFFER</a></h4>
            </div>
            <div class="col-md-6 sofa-grid sofs">
                <img src="../../../Resource/front_end_asset/images/t2.jpg" alt=""/>
                <h3>SPECIAL DESIGN SOFAS</h3>
                <h4><a href="products.php">FABFURNISHING MELA</a></h4>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!---->
<div class="new">
    <div class="container">
        <h3>specially designed for Furnyish</h3>
        <div class="new-products">
            <div class="new-items">
                <div class="item1">
                    <a href="products.php"><img src="../../../Resource/front_end_asset/images/s1.jpg" alt=""/></a>
                    <div class="item-info">
                        <h4><a href="products.php">Brown Furny Seater</a></h4>
                        <span>ID: SR5421</span>
                        <a href="single.php">Buy Now</a>
                    </div>
                </div>
                <div class="item4">
                    <a href="products.php"><img src="../../../Resource/front_end_asset/images/s4.jpg" alt=""/></a>
                    <div class="item-info4">
                        <h4><a href="products.php">Dream Furniture Bed</a></h4>
                        <span>ID: SR5421</span>
                        <a href="single.php">Buy Now</a>
                    </div>			 
                </div>
            </div>
            <div class="new-items new_middle">
                <div class="item2">			 
                    <div class="item-info2">
                        <h4><a href="products.php">Georgia Sofa Set</a></h4>
                        <span>ID: GS7641</span>
                        <a href="single.php">Buy Now</a>
                    </div>
                    <a href="products.php"><img src="../../../Resource/front_end_asset/images/s2.jpg" alt=""/></a>
                </div>
                <div class="item5">	
                    <a href="products.php"><img src="../../../Resource/front_end_asset/images/s5.jpg" alt=""/></a>
                    <div class="item-info5">
                        <h4><a href="products.php">BlackBurn Law Set</a></h4>
                        <span>ID: SR5421</span>
                        <a href="single.php">Buy Now</a>
                    </div>	
                </div>
            </div>		  
            <div class="new-items new_last">
                <div class="item3">	
                    <a href="products.php"><img src="../../../Resource/front_end_asset/images/s3.jpg" alt=""/></a>
                    <div class="item-info3">
                        <h4><a href="products.php">Shefan Dinning Set</a></h4>
                        <span>ID: SR5421</span>
                        <a href="single.php">Buy Now</a>
                    </div>			 
                </div>
                <div class="item6">	
                    <a href="products.php"><img src="../../../Resource/front_end_asset/images/s6.jpg" alt=""/></a>
                    <div class="item-info6">
                        <h4><a href="products.php">Irony Sofa Set</a></h4>
                        <span>ID: SR5421</span>
                        <a href="single.php">Buy Now</a>
                    </div>

                </div>
            </div>
            <div class="clearfix"></div>	
        </div>
    </div>		 
</div>
<!---->
<div class="top-sellers">
    <div class="container">
        <h3>TOP - SELLERS</h3>
        <div class="seller-grids">
            <div class="col-md-3 seller-grid">
                <a href="products.php"><img src="../../../Resource/front_end_asset/images/ts2.jpg" alt=""/></a>
                <h4><a href="products.php">Carnival Doublecot Bed</a></h4>
                <span>ID: DB4790</span>
                <p>Rs. 25000/-</p>
            </div>
            <div class="col-md-3 seller-grid">
                <a href="products.php"><img src="../../../Resource/front_end_asset/images/ts11.jpg" alt=""/></a>
                <h4><a href="products.php">Home Bar Furniture</a></h4>
                <span>ID: BR4822</span>
                <p>Rs. 5000/-</p>
            </div>
            <div class="col-md-3 seller-grid">
                <a href="products.php"><img src="../../../Resource/front_end_asset/images/ts3.jpg" alt=""/></a>
                <h4><a href="products.php">L-shaped Fabric Sofa set</a></h4>
                <span>ID: LF8560</span>
                <p>Rs. 45000/-</p>
            </div>
            <div class="col-md-3 seller-grid">
                <a href="products.php"><img src="../../../Resource/front_end_asset/images/ts4.jpg" alt=""/></a>
                <h4><a href="products.php">Ritz Glass Dinning Table </a></h4>
                <span>ID: DB4790</span>
                <p>Rs. 18000/-</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!---->
<div class="recommendation">
    <div class="container">
        <div class="recmnd-head">
            <h3>RECOMMENDATIONS FOR YOU</h3>
        </div>
        <div class="bikes-grids">			 
            <ul id="flexiselDemo1">
                <?php foreach ($products as $product) { ?>	
                <li>
                    <a href="single.php?product_id=<?php echo $product->product_id ?>"><img src="<?php echo $product->image_1; ?>" alt="HTML5 Icon" width="138" height="128"></a>	
                    <h4><a href="single.php?product_id=<?php echo $product->product_id ?>"><?php echo $product->product_name; ?></a></h4>	
                    <p>ID: <?php echo $product->product_code; ?></p>
                </li>
                <?php } ?>	
            </ul>
            <script type="text/javascript">
                $(window).load(function () {
                    $("#flexiselDemo1").flexisel({
                        visibleItems: 5,
                        animationSpeed: 1000,
                        autoPlay: true,
                        autoPlaySpeed: 3000,
                        pauseOnHover: true,
                        enableResponsiveBreakpoints: true,
                        responsiveBreakpoints: {
                            portrait: {
                                changePoint: 480,
                                visibleItems: 1
                            },
                            landscape: {
                                changePoint: 640,
                                visibleItems: 2
                            },
                            tablet: {
                                changePoint: 768,
                                visibleItems: 3
                            }
                        }
                    });
                });
            </script>
            <script type="text/javascript" src="../../../Resource/front_end_asset/js/jquery.flexisel.js"></script>			 
        </div>
    </div>
</div>


<?php include 'front_end_layout/footer.php'; ?>

