

<?php //include 'front_end_layout/header.php';  ?>
<?php include 'navmanu.php'; ?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniPro' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php'); //using absolute path
//include_once('vendor/autoload.php');
//include_once('../../startup.php');

use APP\BITM\PHP_Soldiers\FURNITURE\Product\Product;
use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

//Utility::prx($_GET);

$product = new Product();
$products = $product->get_single_id_details($_GET['product_id']);

//Utility::prx($products->product_name);

?>


<div class="single-sec">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="active">Products</li>
        </ol>
        <!-- start content -->	
        <div class="col-md-9 det">
            <div class="single_left">
                <div class="grid images_3_of_2">
                    <ul id="etalage" style="text-decoration: none;">
                        <li>
                            <img src="<?php echo $products->image_1; ?>" alt="HTML5 Icon" width="300" height="230">
                        </li>							
                    </ul>
                    <div class="clearfix"></div>		
                </div>
            </div>
            <div class="single-right">
                <h3><?php echo $products->product_name; ?></h3>
                <div class="id"><h4>ID: <?php echo $products->product_code; ?></h4></div>
                <form action="" class="sky-form">
                    <fieldset>					
                        <section>
                            <div class="rating">
                                <input type="radio" name="stars-rating" id="stars-rating-5">
                                <label for="stars-rating-5"><i class="icon-star"></i></label>
                                <input type="radio" name="stars-rating" id="stars-rating-4">
                                <label for="stars-rating-4"><i class="icon-star"></i></label>
                                <input type="radio" name="stars-rating" id="stars-rating-3">
                                <label for="stars-rating-3"><i class="icon-star"></i></label>
                                <input type="radio" name="stars-rating" id="stars-rating-2">
                                <label for="stars-rating-2"><i class="icon-star"></i></label>
                                <input type="radio" name="stars-rating" id="stars-rating-1">
                                <label for="stars-rating-1"><i class="icon-star"></i></label>
                                <div class="clearfix"></div>
                            </div>
                        </section>
                    </fieldset>
                </form>
                <div class="cost">
                    <div class="prdt-cost">
                        <ul>
                            <li>MRP: <del>Rs 55000</del></li>								 
                            <li>Sellling Price:</li>
                            <li class="active">Rs <?php echo $products->product_price; ?></li>
                            <a href="#">BUY NOW</a>
                        </ul>
                    </div>
                    <div class="check">
                        <p><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>Enter pin code for delivery & availability</p>
                        <form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Enter Pin code">
                            </div>
                            <button type="submit" class="btn btn-default">Verify</button>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="item-list">
                    <ul>
                        <li>Material: Solid Wood</li>
                        <li>Color: Brown</li>
                        <li>Style: Solid Wood</li>
                        <li>Brand: Corelle</li>
                        <li><a href="#">Click here for more details</a></li>
                    </ul>
                </div>
                <div class="single-bottom1">
                    <h6>Details</h6>
                    <p class="prod-desc"><?php echo $products->description; ?></p>
                </div>					 
            </div>
            <div class="clearfix"></div>
            <div class="sofaset-info">
                <h4>Product Summary SPENCER 3+1+1 SOFA SET WITH 5 BIG CUSHIONS & WOODEN HANDLE</h4>
                <ul>
                    <li>Dimensions: 3 Seater: Length 208 x Width 81 x Height 91.5 cm and 1 Seater: Length 99 x Width 81 x Height 91.5 cm</li>
                    <li>Assembly Type: Pre Assembled</li>
                    <li>Material: Wooden Structure, Chemical Treated And Seasoned Wood, 19 mm and 12 mm Commercial Ply and Mdf Used On Visible Parts</li>
                    <li>Looks amazing in a contemporary setting</li>
                    <li>Colour: Brown Jute, Sheron Brown</li>
                    <li>Type: Spencer 3 plus 1 plus 1 Sofa Set With 5 Big Cushions and 6 Small Cushions, Wooden Handle</li>
                    <li>Long lasting, durable and easy to use product</li>
                    <li>Contents: 3 Pc</li>
                    <li>Delivery Time: 7 to 10 days from the Day of Dispatch</li>
                    <li>Very classy and contemporary design</li>
                    <li>SUPC: SHG21458689652</li>
                    <li>Material: High Density Foam and Fabric</li>
                </ul>
            </div>				  					
        </div>
        <div class="rsidebar span_1_of_left">
            <section  class="sky-form">
                <div class="product_right">
                    <h4 class="m_2"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span>Categories</h4>
                    <div class="tab1">
                        <ul class="place">								
                            <li class="sort">Furniture</li>
                            <li class="by"><img src="Resource/front_end_asset/images/do.png" alt=""></li>
                            <div class="clearfix"> </div>
                        </ul>
                        <div class="single-bottom">						
                            <a href="#"><p>Sofas</p></a>
                            <a href="#"><p>Fabric Sofas</p></a>
                            <a href="#"><p>Love Seats</p></a>
                            <a href="#"><p>Dinning Sets</p></a>
                        </div>
                    </div>						  
                    <div class="tab2">
                        <ul class="place">								
                            <li class="sort">Decor</li>
                            <li class="by"><img src="Resource/front_end_asset/images/do.png" alt=""></li>
                            <div class="clearfix"> </div>
                        </ul>
                        <div class="single-bottom">						
                            <a href="#"><p>Shelves</p></a>
                            <a href="#"><p>Wall Racks</p></a>
                            <a href="#"><p>Curios</p></a>
                            <a href="#"><p>Ash Trays</p></a>
                        </div>
                    </div>
                    <div class="tab3">
                        <ul class="place">								
                            <li class="sort">Lighting</li>
                            <li class="by"><img src="Resource/front_end_asset/images/do.png" alt=""></li>
                            <div class="clearfix"> </div>
                        </ul>
                        <div class="single-bottom">						
                            <a href="#"><p>Table Lamps</p></a>
                            <a href="#"><p>Tube Lights</p></a>
                            <a href="#"><p>Study Lamps</p></a>
                            <a href="#"><p>Usb Lamps</p></a>
                        </div>
                    </div>
                    <div class="tab4">
                        <ul class="place">								
                            <li class="sort">Bed & Bath</li>
                            <li class="by"><img src="Resource/front_end_asset/images/do.png" alt=""></li>
                            <div class="clearfix"> </div>
                        </ul>
                        <div class="single-bottom">						
                            <a href="#"><p>Towels</p></a>
                            <a href="#"><p>Bath Roles</p></a>
                            <a href="#"><p>Mirrors</p></a>
                            <a href="#"><p>Soap Stands</p></a>
                        </div>
                    </div>
                    <div class="tab5">
                        <ul class="place">								
                            <li class="sort">Fabric</li>
                            <li class="by"><img src="Resource/front_end_asset/images/do.png" alt=""></li>
                            <div class="clearfix"> </div>
                        </ul>
                        <div class="single-bottom">						
                            <a href="#"><p>Sofas</p></a>
                            <a href="#"><p>Fabric Sofas</p></a>
                            <a href="#"><p>Beds</p></a>
                            <a href="#"><p>Relax Chairs</p></a>
                        </div>
                    </div>

                    <!--script-->
                    <script>
                        $(document).ready(function () {
                            $(".tab1 .single-bottom").hide();
                            $(".tab2 .single-bottom").hide();
                            $(".tab3 .single-bottom").hide();
                            $(".tab4 .single-bottom").hide();
                            $(".tab5 .single-bottom").hide();

                            $(".tab1 ul").click(function () {
                                $(".tab1 .single-bottom").slideToggle(300);
                                $(".tab2 .single-bottom").hide();
                                $(".tab3 .single-bottom").hide();
                                $(".tab4 .single-bottom").hide();
                                $(".tab5 .single-bottom").hide();
                            })
                            $(".tab2 ul").click(function () {
                                $(".tab2 .single-bottom").slideToggle(300);
                                $(".tab1 .single-bottom").hide();
                                $(".tab3 .single-bottom").hide();
                                $(".tab4 .single-bottom").hide();
                                $(".tab5 .single-bottom").hide();
                            })
                            $(".tab3 ul").click(function () {
                                $(".tab3 .single-bottom").slideToggle(300);
                                $(".tab4 .single-bottom").hide();
                                $(".tab5 .single-bottom").hide();
                                $(".tab2 .single-bottom").hide();
                                $(".tab1 .single-bottom").hide();
                            })
                            $(".tab4 ul").click(function () {
                                $(".tab4 .single-bottom").slideToggle(300);
                                $(".tab5 .single-bottom").hide();
                                $(".tab3 .single-bottom").hide();
                                $(".tab2 .single-bottom").hide();
                                $(".tab1 .single-bottom").hide();
                            })
                            $(".tab5 ul").click(function () {
                                $(".tab5 .single-bottom").slideToggle(300);
                                $(".tab4 .single-bottom").hide();
                                $(".tab3 .single-bottom").hide();
                                $(".tab2 .single-bottom").hide();
                                $(".tab1 .single-bottom").hide();
                            })
                        });
                    </script>
                    <!-- script -->					 
            </section>
            <section  class="sky-form">
                <h4><span class="glyphicon glyphicon-minus" aria-hidden="true"></span>DISCOUNTS</h4>
                <div class="row row1 scroll-pane">
                    <div class="col col-4">
                        <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Upto - 10% (20)</label>
                    </div>
                    <div class="col col-4">
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>40% - 50% (5)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>30% - 20% (7)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>10% - 5% (2)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Other(50)</label>
                    </div>
                </div>
            </section> 				  
            <section  class="sky-form">
                <h4><span class="glyphicon glyphicon-minus" aria-hidden="true"></span>Price</h4>
                <ul class="dropdown-menu1">
                    <li><a href="">								                
                            <div id="slider-range"></div>							
                            <input type="text" id="amount" style="border: 0; font-weight: NORMAL;   font-family: 'Arimo', sans-serif;" />
                        </a></li>			
                </ul>
            </section>
            <!---->
            <script type="text/javascript" src="../../../Resource/front_end_asset/js/jquery-ui.min.js"></script>
            <link rel="stylesheet" type="text/css" href="../../../Resource/front_end_asset/css/jquery-ui.css">
            <script type='text/javascript'>//<![CDATA[ 
                        $(window).load(function () {
                            $("#slider-range").slider({
                                range: true,
                                min: 0,
                                max: 400000,
                                values: [2500, 350000],
                                slide: function (event, ui) {
                                    $("#amount").val("$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ]);
                                }
                            });
                            $("#amount").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));

                        });//]]>  

            </script>
            <section  class="sky-form">
                <h4><span class="glyphicon glyphicon-minus" aria-hidden="true"></span>Type</h4>
                <div class="row row1 scroll-pane">
                    <div class="col col-4">
                        <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Sofa Cum Beds (30)</label>
                    </div>
                    <div class="col col-4">
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Dinner Sets   (30)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>3+1+1 Sofa Sets (30)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Sofa Chairs     (30)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>2 Seater Sofas  (30)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Display Units   (30)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>L-shaped Sofas  (30)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>3 Seater Sofas  (30)</label>
                    </div>
                </div>
            </section>
            <section  class="sky-form">
                <h4><span class="glyphicon glyphicon-minus" aria-hidden="true"></span>Brand</h4>
                <div class="row row1 scroll-pane">
                    <div class="col col-4">
                        <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Roadstar</label>
                    </div>
                    <div class="col col-4">
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Tornado</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Kissan</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Oakley</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Manga</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>Wega</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Kings</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Zumba</label>
                    </div>
                </div>
            </section>			  									 			 
        </div>
    </div>	     				
    <div class="clearfix"></div>
</div>	 
</div>

<?php include 'front_end_layout/footer.php'; ?>

