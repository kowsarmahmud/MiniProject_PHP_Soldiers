

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniPro' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php'); //using absolute path
//include_once('vendor/autoload.php');
//include_once('../../startup.php');

use APP\BITM\PHP_Soldiers\FURNITURE\Category\Category;
use APP\BITM\PHP_Soldiers\FURNITURE\Sub_Category\Sub_Category;
use APP\BITM\PHP_Soldiers\FURNITURE\Product\Product;
use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

$category = new Category();
$categorys = $category->select_all_published_category();
//Utility::prx($categorys);

$sub_category = new Sub_Category();
$sub_categorys = $sub_category->select_all_published_sub_category();
//Utility::prx($sub_categorys);
$product = new Product();
$products = $product->select_all_published_product();
//Utility::prx($products);
?>

<?php include 'front_end_layout/header.php'; ?>

<div class="mega_nav">
    <div class="container">
        <div class="menu_sec">
            <!-- start header menu -->
            <ul class="megamenu skyblue">
                <li class="active grid"><a class="color1" href="index.php">Home</a></li>
                <?php foreach ($categorys as $category) { ?>
                <li class="grid"><a class="color2" href="#"><?php  echo $category->category_name; ?></a>
                    <div class="megapanel">
                        <div class="row">
                            <div class="col1">
                                <div class="h_nav">
                                    <?php foreach ($sub_categorys as $sub_category) { ?>
                                    <h4><a href="products.php?sub_category_id=<?php echo $sub_category->sub_category_id; ?>"><?php echo $sub_category->sub_category_name; ?></a></h4>
                                    <ul>
                                        <?php foreach ($products as $product) { ?>
                                        <li><a href="single.php?product_id=<?php echo $product->product_id ?>"><?php  echo $product->product_name; ?></a></li>
                                        <?php } // Product ?>
                                    </ul>	
                                    <?php } // sub category ?>
                                </div>							
                            </div>
                        </div>
                        <div class="row">
                            <div class="col2"></div>
                            <div class="col1"></div>
                            <div class="col1"></div>
                            <div class="col1"></div>
                            <div class="col1"></div>
                        </div>
                    </div>
                </li>
                <?php } //  // category ?> 
            </ul> 
            <div class="search">
                <form>
                    <input type="text" value="" placeholder="Search...">
                    <input type="submit" value="">
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>