<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniPro' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php'); //using absolute path
//include_once('vendor/autoload.php');
//include_once('../../startup.php');

use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

if (!isset($_SESSION['username']) or !isset($_SESSION['password'])) {
    header("location: login.php");
    $_message = "Please login";
    Utility::message($_message);
}



?>
<?php include 'layout/header.php'; ?>

<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.php">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">View</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Sub Category</h2>
            </div>
            <div class="box-content">
                <form class="form-horizontal">
                    <fieldset>                      
                        <h4>Name: <?php echo $sub_categorys->sub_category_name; ?></h4>
                        <h4>Hobby: <?php echo $sub_categorys->sub_category_id; ?></h4>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->
        
    </div><!--/row-->
    <a class="btn btn-success" href="category_index.php">Go to List</a>
    <a class="btn btn-success" href="javascript:history.go(-1)">Back</a>






</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->


<?php include 'layout/footer.php'; ?>