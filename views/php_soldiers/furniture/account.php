

<?php //include 'front_end_layout/header.php'; ?>
<?php include 'navmanu.php'; ?>

<div class="container">
    <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li class="active">Account</li>
    </ol>
    <div class="registration">
        <div class="registration_left">
            <h2>new user? <span> create an account </span></h2>
            <!-- [if IE] 
                   < link rel='stylesheet' type='text/css' href='ie.css'/>  
            [endif] -->  

            <!-- [if lt IE 7]>  
                   < link rel='stylesheet' type='text/css' href='ie6.css'/>  
            <! [endif] -->  
            <script>
                (function () {

                    // Create input element for testing
                    var inputs = document.createElement('input');

                    // Create the supports object
                    var supports = {};

                    supports.autofocus = 'autofocus' in inputs;
                    supports.required = 'required' in inputs;
                    supports.placeholder = 'placeholder' in inputs;

                    // Fallback for autofocus attribute
                    if (!supports.autofocus) {

                    }

                    // Fallback for required attribute
                    if (!supports.required) {

                    }

                    // Fallback for placeholder attribute
                    if (!supports.placeholder) {

                    }

                    // Change text inside send button on submit
                    var send = document.getElementById('register-submit');
                    if (send) {
                        send.onclick = function () {
                            this.innerHTML = '...Sending';
                        }
                    }

                })();
            </script>
            <div class="registration_form">
                <!-- Form -->
                <form id="registration_form" action="account_create.php" method="POST">
                    <div>
                        <label>
                            <input placeholder="first name:" 
                                   type="text" 
                                   name="first_name"
                                   tabindex="1" 
                                   required autofocus>
                        </label>
                    </div>
                    <div>
                        <label>
                            <input placeholder="last name:" 
                                   type="text" 
                                   name="last_name"
                                   tabindex="2" r
                                   equired autofocus>
                        </label>
                    </div>
                    <div>
                        <label>
                            <input placeholder="email address:" 
                                   type="email" 
                                   name="email"
                                   tabindex="4" 
                                   required>
                        </label>
                    </div>
                    <div>
                        <label>
                            <input placeholder="Mobile:" 
                                   type="number" 
                                   name="mobile"
                                   tabindex="5" r
                                   equired>
                        </label>
                    </div>					
                    <div class="sky_form1">
                        <ul>
                            <li><label class="radio left"><input type="radio" 
                                                                 name="gender" 
                                                                 value="Male"
                                                                 checked=""><i></i>Male</label></li>
                            <li><label class="radio"><input type="radio" 
                                                            value="Female"
                                                            name="gender"><i></i>Female</label></li>
                            <div class="clearfix"></div>
                        </ul>
                    </div>					
                    <div>
                        <label>
                            <input placeholder="password" 
                                   type="password" 
                                   name="password"
                                   tabindex="6" 
                                   required>
                        </label>
                    </div>						
                    <div>
                        <label>
                            <input placeholder="retype password" 
                                   type="password" 
                                   name="retype_password"
                                   tabindex="7" 
                                   required>
                        </label>
                    </div>	
                    <div>
                        <input type="submit" 
                               name="agree" 
                               tabindex="8" 
                               id="register-submit">
                    </div>
                    <div class="sky-form">
                        <label class="checkbox"><input type="checkbox" 
                                                       name="checkbox" >
                            <i></i>i agree to mobilya.com &nbsp;<a class="terms" href="#"> terms of service</a> </label>
                    </div>
                </form>
                <!-- /Form -->
            </div>
        </div>
        <div class="registration_left">
            <h2>existing user</h2>
            <div class="registration_form">
                <!-- Form -->
                <form id="registration_form" action="contact.php" method="post">
                    <div>
                        <label>
                            <input placeholder="email:" type="email" tabindex="3" required>
                        </label>
                    </div>
                    <div>
                        <label>
                            <input placeholder="password" type="password" tabindex="4" required>
                        </label>
                    </div>						
                    <div>
                        <input type="submit" value="sign in" id="register-submit">
                    </div>
                    <div class="forget">
                        <a href="#">forgot your password</a>
                    </div>
                </form>
                <!-- /Form -->
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<?php include 'front_end_layout/footer.php'; ?>





