

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniPro' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php'); //using absolute path
//include_once('vendor/autoload.php');
//include_once('../../startup.php');

use APP\BITM\PHP_Soldiers\FURNITURE\Sub_Category\Sub_Category;
use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

//Utility::prx($_POST);

if (!isset($_SESSION['username']) or !isset($_SESSION['password'])) {
    header("location: login.php");
    $_message = "Please login";
    Utility::message($_message);
}


$sub_category = new Sub_Category();

if (isset($_GET['status'])) {
    if ($_GET['status'] == 'unpublished') {
        $sub_category_id = $_GET['sub_category_id'];
        $message = $sub_category->published($sub_category_id);
    } else if ($_GET['status'] == 'published') {
        $sub_category_id = $_GET['sub_category_id'];
        $message = $sub_category->unpublished($sub_category_id);
    }
}
?>  