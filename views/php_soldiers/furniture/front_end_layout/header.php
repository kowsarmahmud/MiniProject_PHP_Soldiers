
<!DOCTYPE HTML>
<html>
    <head>
        <title>Furnyish Store a Ecommerce Category Flat</title>
        <link href="../../../Resource/front_end_asset/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- jQuery (necessary JavaScript plugins) -->
        <script type='text/javascript' src="../../../Resource/front_end_asset/js/jquery-1.11.1.min.js"></script>
        <!-- Custom Theme files -->
        <link href="../../../Resource/front_end_asset/css/style.css" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <!--//theme-style-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Furnyish Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href='http://fonts.googleapis.com/css?family=Montserrat|Raleway:400,200,300,500,600,700,800,900,100' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Aladin' rel='stylesheet' type='text/css'>
        <!-- start menu -->
        <link href="../../../Resource/front_end_asset/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
        <script type="text/javascript" src="../../../Resource/front_end_asset/js/megamenu.js"></script>
        <script>$(document).ready(function () {
                $(".megamenu").megamenu();
            });</script>
        <script src="../../../Resource/front_end_asset/js/menu_jquery.js"></script>
        <script src="../../../Resource/front_end_asset/js/simpleCart.min.js"></script>
        <script src="../../../Resource/front_end_asset/js/responsiveslides.min.js"></script>
        <script>
            // You can also use "$(window).load(function() {"
            $(function () {
                // Slideshow 1
                $("#slider1").responsiveSlides({
                    auto: true,
                    nav: true,
                    speed: 500,
                    namespace: "callbacks",
                });
            });
        </script>

    </head>
    <body>
        <!-- header -->
        <div class="top_bg">
            <div class="container">
                <div class="header_top-sec">
                    <div class="top_right">
                        <ul>
                            <li><a href="#">help</a></li>|
                            <li><a href="contact.php">Contact</a></li>|
                            <li><a href="login.php">Track Order</a></li>
                        </ul>
                    </div>
                    <div class="top_left">
                        <ul>
                            <li class="top_link">Email:<a href="mailto@example.com">info(at)Mobilya.com</a></li>|
                            <li class="top_link"><a href="login.php">My Account</a></li>|					
                        </ul>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="facebook"></i></a></li>
                                <li><a href="#"><i class="twitter"></i></a></li>
                                <li><a href="#"><i class="dribble"></i></a></li>	
                                <li><a href="#"><i class="google"></i></a></li>										
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <div class="header_top">
            <div class="container">
                <div class="logo">
                    <a href="index.php"><img src="../../../Resource/front_end_asset/images/logo.png" alt=""/></a>			 
                </div>
                <div class="header_right">	
                    <div class="login">
                        <a href="login.php">LOGIN</a>
                    </div>
                    <div class="cart box_1">
                        <a href="cart.php">
                            <h3> <span class="simpleCart_total">$0.00</span> (<span id="simpleCart_quantity" class="simpleCart_quantity">0</span> items)<img src="../../../Resource/front_end_asset/images/bag.png" alt=""></h3>
                        </a>	
                        <p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>
                        <div class="clearfix"> </div>
                    </div>				 
                </div>
                <div class="clearfix"></div>	
            </div>
        </div>
        <!--cart-->
        <?php //include 'navmanu.php'; ?>
        <!------>

