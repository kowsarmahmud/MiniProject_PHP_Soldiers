

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniPro' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php'); //using absolute path
//include_once('vendor/autoload.php');
//include_once('../../startup.php');

use APP\BITM\PHP_Soldiers\FURNITURE\Category\Category;
use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

if (!isset($_SESSION['username']) or !isset($_SESSION['password'])) {
    header("location: login.php");
    $_message = "Please login";
    Utility::message($_message);
}

//Utility::prx($_POST);

$category = new Category();

if (isset($_GET['status'])) {
    if ($_GET['status'] == 'unpublished') {
        $category_id = $_GET['category_id'];
        $message = $category->published($category_id);
    } else if ($_GET['status'] == 'published') {
        $category_id = $_GET['category_id'];
        $message = $category->unpublished($category_id);
    }
}
?>  