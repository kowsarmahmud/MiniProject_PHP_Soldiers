<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniPro' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php'); //using absolute path
//include_once('vendor/autoload.php');
//include_once('../../startup.php');

use APP\BITM\PHP_Soldiers\FURNITURE\Sub_Category\Sub_Category;
use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

if (!isset($_SESSION['username']) or !isset($_SESSION['password'])) {
    header("location: login.php");
    $_message = "Please login";
    Utility::message($_message);
}

//Utility::prx($_GET);
$sub_category = new Sub_Category();
$sub_categorys= $sub_category->get_single_id_details($_GET['sub_category_id']);

//Utility::prx($sub_categorys);
?>
<?php include 'layout/header.php'; ?>

<!-- start: Content -->
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="sub_category_index.php">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Forms</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>User Edit</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form action="sub_category_update.php" method="post" class="form-horizontal">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="title">Sub Category Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused"
                                       autofocus="autofocus" 
                                       id="title" 
                                       type="text" 
                                       name="sub_category_name"
                                       value="<?php echo $sub_categorys->sub_category_name; ?>"
                                       tabindex="1"
                                       placeholder="input sub category name"
                                       required="required" >
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <input type="hidden"  name="sub_category_id" value="<?php echo $sub_categorys->sub_category_id; ?>" >
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" tabindex="4" class="btn btn-primary">Save</button>
                            <input tabindex="6" class="btn" type="reset" value="Reset" />
                        </div>
                    </fieldset>
                </form>   
            </div>
        </div><!--/span-->
        <a class="btn btn-success" href="index.php">Go to List</a>
        <a class="btn btn-success" href="javascript:history.go(-1)">Back</a>

    </div><!--/row-->






</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<?php include 'layout/footer.php'; ?>