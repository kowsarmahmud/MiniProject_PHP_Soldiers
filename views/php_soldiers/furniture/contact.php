

<?php //include 'front_end_layout/header.php'; ?>
<?php include 'navmanu.php'; ?>


<div class="contact">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li class="active">Contact</li>
        </ol>
        <div class="contact-head">
            <h2>CONTACT</h2>
            <form action="contact_Mail.php" method="POST" >
                <div class="col-md-6 contact-left">
                    <input type="text" name="name" placeholder="Name" required/>
                    <input type="text" name="email" placeholder="E-mail" required/>
                    <input type="text" name="phone" placeholder="Phone" required/>
                </div>
                <div class="col-md-6 contact-right">
                    <textarea name="message" placeholder="Message"></textarea>
                    <input type="submit" name="submit" value="SEND"/>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
        <div class="address">
            <h3>Our Locations</h3>
            <div class="locations">				 
                <ul>
                    <li><span></span></li>					 					
                    <li>
                        <div class="address-info">	
                            <h4>New York, Washington</h4>
                            <p>10-765 MD-Road</p>
                            <p>Washington, DC, United States,</p>
                            <p>Phone: 123 456 7890</p>
                            <p>Mail: <a href="mailto:info@example.com">info(at)example.com</a></p>
                            <h5><a href="">Visit on Google Maps >></a></h5>	
                        </div>
                    </li>				
                </ul>	
                <ul>
                    <li><span></span></li>					 					
                    <li>
                        <div class="address-info">	
                            <h4>London, UK</h4>
                            <p>10-765 MD-Road</p>
                            <p>Lorem ipsum, domon sit, UK,</p>
                            <p>Phone: 123 456 7890</p>
                            <p>Mail: <a href="mailto:info@example.com">info(at)example.com</a></p>
                            <h5><a href="">Visit on Google Maps >></a></h5>	
                        </div>
                    </li>				
                </ul>		
            </div>			 
        </div>
        
    </div>
</div>



<?php include 'front_end_layout/footer.php'; ?>

