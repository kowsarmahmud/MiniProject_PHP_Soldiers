<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniPro' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php'); //using absolute path
//include_once('vendor/autoload.php');
//include_once('../../startup.php');

use APP\BITM\PHP_Soldiers\FURNITURE\Sub_Category\Sub_Category;
use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

if (!isset($_SESSION['username']) or !isset($_SESSION['password'])) {
    header("location: login.php");
    $_message = "Please login";
    Utility::message($_message);
}

//Utility::prx($_POST);
$sub_category = new Sub_Category($_POST);
$sub_categorys= $sub_category->update_single_id_details();
?>