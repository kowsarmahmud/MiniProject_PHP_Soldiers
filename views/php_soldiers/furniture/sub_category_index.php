<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniPro' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php'); //using absolute path
//include_once('vendor/autoload.php');
//include_once('../../startup.php');

use APP\BITM\PHP_Soldiers\FURNITURE\Sub_Category\Sub_Category;
use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

if (!isset($_SESSION['username']) or ! isset($_SESSION['password'])) {
    header("location: login.php");
    $_message = "Please login";
    Utility::message($_message);
}

$sub_category = new Sub_Category();
$sub_categorys = $sub_category->index();
//Utility::prx($sub_categorys);
?>
<?php include 'layout/header.php'; ?>

<!-- start: Content -->
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="sub_category_index.php">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Tables</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Sub Category Panel</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <h3><?php echo Utility::message()?></h3>
            <div class="box-content">

                <table class="table table-striped table-bordered bootstrap-datatable datatable">

                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Active</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $num = 1;
                        foreach ($sub_categorys as $sub_category) {
                            ?>
                            <tr>
                                <td><?php echo $num; ?></td>
                                <td><?php echo $sub_category->sub_category_name; ?></td>
                                <?php
                                if ($sub_category->deletion_status == 0) {
                                    ?>
                                    <td class="center">
                                        <span class="label label-success">Active</span>
                                    </td>
                                    <?php
                                } else {
                                    ?>
                                    <td class="center">
                                        <span class="label label-warning">Banned</span>
                                    </td>
                                    <?php
                                }
                                ?>
                                <td class="center">
                                    <a class="btn btn-success" title="View" href="sub_category_view.php?sub_category_id=<?php echo $sub_category->sub_category_id ?>">
                                        <i class="halflings-icon white zoom-in"></i>  
                                    </a>
                                    <a class="btn btn-info" title="Edit" href="sub_category_edit.php?sub_category_id=<?php echo $sub_category->sub_category_id ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                    <a class="btn btn-danger" class="delete" title="Delete"  href="sub_category_delete.php?sub_category_id=<?php echo $sub_category->sub_category_id ?>" onclick="return check();">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                    <?php
                                    if ($sub_category->deletion_status == 0) {
                                        ?>
                                        <a style="width: 60px; height: 30px; background-color: green; color: white; padding: 8px;" title="Trash" href="sub_category_trash.php?status=unpublished&sub_category_id=<?php echo $sub_category->sub_category_id ?>">Trash</a>
                                        <?php
                                    } else {
                                        ?>
                                        <a style="width: 50px; height: 50px; background-color: red; color: white; padding: 8px;" title="Recover" href="sub_category_trash.php?status=published&sub_category_id=<?php echo $sub_category->sub_category_id ?>">Recover</a>
                                        <?php
                                    }
                                    ?>

                                </td>
                            </tr>
                            <?php
                            $num++;
                        }
                        ?>

                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->
    <a class="btn btn-success" href="sub_category_create.php">Add Sub Category</a><br>

</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<script type="text/javascript" src="js/jquery.js"></script>
<script>
                                    function check() {
                                        var chk = confirm("Are you sure to Delete?");
                                        if (chk) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    }
</script>

<?php include 'layout/footer.php'; ?>