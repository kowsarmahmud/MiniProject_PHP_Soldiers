
<?php

include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniPro' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php'); //using absolute path
//include_once('vendor/autoload.php');
//include_once('../../startup.php');

//use APP\BITM\PHP_Soldiers\FURNITURE\User\User;
use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;


?>



<?php //include 'front_end_layout/header.php'; ?>
<?php include 'navmanu.php'; ?>

<div class="login_sec">
	 <div class="container">
		 <ol class="breadcrumb">
		  <li><a href="index.php">Home</a></li>
		  <li class="active">Login</li>
		 </ol>
             <h3 style="margin-left: 50px; color: red;"><?php echo Utility::message()?></h3>
		 <h2>Login</h2>
		 <div class="col-md-6 log">			 
				 <p>Welcome, please enter the folling to continue.</p>
				 <p>If you have previously Login with us, <span>click here</span></p>
                                 <form action="check_user.php" method="POST">
					 <h5>User Name:</h5>	
                                         <input type="text" required="" name="first_name" value="">
					 <h5>Password:</h5>
                                         <input type="password" required="" name="password" value="">					
                                         <input type="submit" name="submit" value="Login">
					 <a href="#">Forgot Password ?</a>
				 </form>				 
		 </div>
		  <div class="col-md-6 login-right">
			  	<h3>NEW REGISTRATION</h3>
				<p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
				<a class="acount-btn" href="account.php">Create an Account</a>
		 </div>
		 <div class="clearfix"></div>
	 </div>
</div>

<?php include 'front_end_layout/footer.php'; ?>

