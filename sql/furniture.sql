-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 23, 2016 at 09:55 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `furniture`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `deletion_status`) VALUES
(47, 'KIDS', 0),
(48, 'DECOR', 0),
(49, 'APPLIANCES', 0),
(50, 'KITCHEN DINNING', 0),
(51, 'LIVING', 0),
(52, 'FURNITURE', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0',
  `product_code` varchar(11) DEFAULT NULL,
  `product_price` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image_1` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `category_id`, `sub_category_id`, `product_name`, `deletion_status`, `product_code`, `product_price`, `description`, `image_1`) VALUES
(10, 52, 20, 'Seater sofa set', 0, 'SR4598', '187', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam \r\nnonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat \r\nvolutpat.    \r\n                                ', '../../../Resource/Uploads/p1.jpg'),
(11, 52, 20, 'Fabric Sofa set', 0, 'SR4598', '187', 'Ut wisi enim ad minim veniam, ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit    \r\n                                ', '../../../Resource/Uploads/p2.jpg'),
(12, 52, 20, 'King Dior Bed', 0, 'SR4598', '187', 'Ut wisi enim ad minim veniam, ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit    \r\n                                ', '../../../Resource/Uploads/p3.jpg'),
(13, 52, 20, 'Newlook Sofa Set', 0, 'SR4598', '987', 'Ut wisi enim ad minim veniam, ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit    \r\n                                ', '../../../Resource/Uploads/p4.jpg'),
(14, 52, 20, 'Maxico Sofa Set', 0, 'SR4598', '187', 'Ut wisi enim ad minim veniam, ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit    \r\n                                ', '../../../Resource/Uploads/p5.jpg'),
(16, 51, 19, 'CozyWilm Sofa Set', 0, 'SR4598', '187', 'Ut wisi enim ad minim veniam, ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit    \r\n                                ', '../../../Resource/Uploads/ss2.jpg'),
(17, 51, 19, 'Table', 0, 'SR4598', '187', 'Ut wisi enim ad minim veniam, ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit    \r\n                                ', '../../../Resource/Uploads/ts4.jpg'),
(18, 48, 19, 'Bed Room', 0, 'SR4598', '187', 'Ut wisi enim ad minim veniam, ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit    \r\n                                ', '../../../Resource/Uploads/s5.jpg'),
(20, 51, 17, 'Rory 5 Seater', 0, 'SR4598', '187', 'Ut wisi enim ad minim veniam, ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit    \r\n                                ', '../../../Resource/Uploads/s3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE IF NOT EXISTS `sub_category` (
  `sub_category_id` int(11) NOT NULL,
  `sub_category_name` varchar(255) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`sub_category_id`, `sub_category_name`, `deletion_status`) VALUES
(16, 'SOLID WOODS', 0),
(17, 'SEATING', 0),
(18, 'kowsar999', 0),
(19, 'TABLES', 0),
(20, 'SOFAS', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(500) NOT NULL,
  `mobile` int(255) NOT NULL,
  `gender` varchar(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `retype_password` varchar(255) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `email`, `mobile`, `gender`, `password`, `retype_password`, `deletion_status`) VALUES
(6, 'Kowsar ', 'Mahmud', 'kawsar29@gmail.com', 1922257846, 'Male', '62616024kl', '62616024kl', 0),
(7, 'PHP_Soldiers', 'Basis', 'mohinu@gmail.com', 123456789, 'Male', 'PHP_Soldiers', 'PHP_Soldiers', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`sub_category_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `sub_category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
