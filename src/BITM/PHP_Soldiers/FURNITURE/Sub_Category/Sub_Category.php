<?php

namespace APP\BITM\PHP_Soldiers\FURNITURE\Sub_Category;

use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

class Sub_Category {

    public $table = "sub_category";
    public $sub_category_name;
    public $sub_category_id;

    public function __construct($data = false) {
        $this->sub_category_id = $data['sub_category_id'];
        $this->sub_category_name = $data['sub_category_name'];

        $this->open_connection();
    }

    public function open_connection() {
        $this->connection = mysql_connect("localhost", "root", "");
        if (!$this->connection) {
            die("Database connection failed: " . mysql_error());
        } else {
            $db_select = mysql_select_db("furniture", $this->connection);
            if (!$db_select) {
                die("Database selection failed: " . mysql_error());
            }
        }
    }

    public function close_connection() {
        if (isset($this->connection)) {
            mysql_close($this->connection);
            unset($this->connection);
        }
    }

    public function index() {

        $objs = array();

        $query = "SELECT * FROM " . $this->table . " ORDER BY sub_category_id DESC";

        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $objs[] = $row;
        }

        return $objs;
    }
    
    public function select_all_published_sub_category() {

        $objs = array();

        $query = "SELECT * FROM " . $this->table . " WHERE `deletion_status` = '0' ORDER BY sub_category_id DESC";
        //Utility::prx($query);
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $objs[] = $row;
        }
        return $objs;
    }

    public function store() {

        $query = "INSERT INTO `" . $this->table . "` (`sub_category_name`) VALUES ( '" . $this->sub_category_name . "')";
        $result = mysql_query($query);

        if ($result) {
            $_message = "". $this->sub_category_name . " is added Sub Category successfully.";
            Utility::message($_message);
            header('Location:sub_category_index.php');
        } else {
            $_message = "There is an error while saving data. Please try again later.";
            Utility::message($_message);
        }
    }

    public function get_single_id_details($id = NULL) {

        $query = "SELECT * FROM " . $this->table . " WHERE sub_category_id = '$id' ";
        $result = mysql_query($query);
        $row = mysql_fetch_object($result);

        return $row;
    }

    public function update_single_id_details() {

        $query = "UPDATE " . $this->table . " SET `sub_category_name` = '" . $this->sub_category_name . "' WHERE `sub_category_id` = " . $this->sub_category_id . "";

        if (mysql_query($query)) {
            $_message = "" . $this->sub_category_name . " is updated Sub Category successfully.";
            Utility::message($_message);
            header('Location:sub_category_index.php');
        } else {
            die('Query problem' . mysql_error());
        }
    }

    public function delete($id = NULL) {

        $query = "DELETE FROM " . $this->table . " WHERE `sub_category_id` = '$id'";

        if (mysql_query($query)) {
            $message = "<h2> Sub Category is deleted successfully.</h2>";
            Utility::message($message);
            header('Location:sub_category_index.php');
        } else {
            die('Query problem' . mysql_error());
        }
    }

    public function published($id = NULL) {

        $quary = "UPDATE `" . $this->table . "` SET `deletion_status` = '1' WHERE `sub_category_id` = '$id .'";
        mysql_query($quary);
        $message = "<h2>Sub Category is unpublished successfully.</h2>";
        Utility::message($message);
        header('Location: sub_category_index.php');
    }

    public function unpublished($id = NULL) {

        $quary = "UPDATE `" . $this->table . "` SET `deletion_status` = '0' WHERE `sub_category_id` = '$id .'";
        mysql_query($quary);
        $message = "<h2>Sub Category is published successfully.</h2>";
        Utility::message($message);
        header('Location: sub_category_index.php');
    }

}
