<?php

namespace APP\BITM\PHP_Soldiers\FURNITURE\Product;
use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

class Product {

    public $table = "product";
    public $product_name;
    public $product_id;
    public $category_id;
    public $sub_category_id;
    public $product_code;
    public $product_price;
    public $description;
    // for images
    public $filetmp;
    public $filename;
    public $filetype;
    public $image_1;

    public function __construct($post = false, $files = false) {
        $this->product_name = $post['product_name'];
        $this->product_id = $post['product_id'];
        $this->category_id = $post['category_id'];
        $this->sub_category_id = $post['sub_category_id'];
        $this->product_code = $post['product_code'];
        $this->product_price = $post['product_price'];
        $this->description = $post['description'];

        $this->filetmp = $files["imgName"]["tmp_name"];
        $this->filename = $files["imgName"]["name"];
        $this->filetype = $files["imgName"]["type"];
        $this->image_1 = "../../../Resource/Uploads/" . $this->filename;

        move_uploaded_file($this->filetmp, $this->image_1);

        $this->open_connection();
    }

    public function open_connection() {
        $this->connection = mysql_connect("localhost", "root", "");
        if (!$this->connection) {
            die("Database connection failed: " . mysql_error());
        } else {
            $db_select = mysql_select_db("furniture", $this->connection);
            if (!$db_select) {
                die("Database selection failed: " . mysql_error());
            }
        }
    }

    public function close_connection() {
        if (isset($this->connection)) {
            mysql_close($this->connection);
            unset($this->connection);
        }
    }

    public function index() {

        $objs = array();

        $query = "SELECT * FROM " . $this->table . " ORDER BY product_id DESC";
        //Utility::prx($query);
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $objs[] = $row;
        }
        return $objs;
    }

    public function select_all_published_product() {

        $objs = array();

        $query = "SELECT * FROM " . $this->table . " WHERE `deletion_status` = '0' ORDER BY product_id DESC";
        //Utility::prx($query);
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $objs[] = $row;
        }
        return $objs;
    }

    public function get_all_product_by_sub_category_id($id = NULL) {

        $query = "SELECT * FROM " . $this->table . " WHERE sub_category_id ='$id' ";
        $result = mysql_query($query);
        $row = mysql_fetch_object($result);

        return $row;
    }

    public function store() {

        $query = "INSERT INTO " . $this->table . " (`product_name`,`category_id`,`sub_category_id`,`product_code`,`product_price`,`description`,`image_1`) VALUES ( '" . $this->product_name . "','" . $this->category_id . "','" . $this->sub_category_id . "','" . $this->product_code . "','" . $this->product_price . "','" . $this->description . "','" . $this->image_1 . "')";
        //Utility::prx($query);
        $result = mysql_query($query);

        if ($result) {
            $_message = "" . $this->product_name . " is added New Product successfully.";
            Utility::message($_message);
            header('Location: product_index.php');
        } else {
            $_message = "There is an error while saving data. Please try again later.";
            Utility::message($_message);
        }
    }

    public function get_single_id_details($id = NULL) {

        $query = "SELECT * FROM " . $this->table . " WHERE product_id ='$id' ";
        $result = mysql_query($query);
        $row = mysql_fetch_object($result);

        return $row;
    }

    public function update_single_id_details() {

        $query = "UPDATE " . $this->table . " SET `product_name` = '" . $this->product_name . "',`category_id` = '" . $this->category_id . "',`sub_category_id` = '" . $this->sub_category_id . "',`product_code` = '" . $this->product_code . "',`product_price` = '" . $this->product_price . "',`description` = '" . $this->description . "',`image_1` = '" . $this->image_1 . "' WHERE `product_id` = " . $this->product_id . "";
        //Utility::prx($query);
        if (mysql_query($query)) {
            $_message = "" . $this->product_name . " Is Update In Product Successfully.";
            Utility::message($_message);
            header('Location: product_index.php');
        } else {
            die('Query problem' . mysql_error());
        }
    }

    public function delete($id = NULL) {

        if (isset($id)) {

            $queryImg = "SELECT * FROM " . $this->table . " WHERE product_id ='$id' ";

            $run = mysql_query($queryImg);
            $row = mysql_fetch_object($run);
            //Utility::prx($row);
            $del_image = $row->image_1;
            unlink($del_image);


            $query = "DELETE FROM " . $this->table . " WHERE `product_id` = '$id'";
            //Utility::prx($query);
            if (mysql_query($query)) {
                $message = "<h2>Product is deleted successfully.</h2>";
                Utility::message($message);
                header('Location: product_index.php');
            } else {
                die('Query problem' . mysql_error());
            }
        }
    }

    public function published($id = NULL) {

        $quary = "UPDATE `" . $this->table . "` SET `deletion_status` = '1' WHERE `product_id` = '$id .'";
        mysql_query($quary);

        $message = "<h2>Product is unpublished successfully.</h2>";
        Utility::message($message);

        header('Location: product_index.php');
    }

    public function unpublished($id = NULL) {

        $quary = "UPDATE `" . $this->table . "` SET `deletion_status` = '0' WHERE `product_id` = '$id .'";
        mysql_query($quary);

        $message = "<h2>Product is published successfully.</h2>";
        Utility::message($message);

        header('Location: product_index.php');
    }

}
