<?php

namespace APP\BITM\PHP_Soldiers\FURNITURE\User;
use APP\BITM\PHP_Soldiers\FURNITURE\Utility\Utility;

class User {

    public $table = "user";
    public $user_id;
    public $first_name;
    public $last_name;
    public $email;
    public $password;
    public $retype_password;
    public $mobile;
    public $gender;

    public function __construct($data = false) {
        $this->user_id = $data['user_id'];
        $this->first_name = $data['first_name'];
        $this->last_name = $data['last_name'];
        $this->email = $data['email'];
        $this->mobile = $data['mobile'];
        $this->gender = $data['gender'];
        $this->password = $data['password'];
        $this->retype_password = $data['retype_password'];

        $this->open_connection();
    }

    public function open_connection() {
        $this->connection = mysql_connect("localhost", "root", "");
        if (!$this->connection) {
            die("Database connection failed: " . mysql_error());
        } else {
            $db_select = mysql_select_db("furniture", $this->connection);
            if (!$db_select) {
                die("Database selection failed: " . mysql_error());
            }
        }
    }

    public function close_connection() {
        if (isset($this->connection)) {
            mysql_close($this->connection);
            unset($this->connection);
        }
    }

    public function store() {

        if ($this->password == $this->retype_password) {

            $query = "INSERT INTO " . $this->table . " (`first_name`,`last_name`,`email`,`mobile`,`gender`,`password`,`retype_password`) VALUES ( '" . $this->first_name . "','" . $this->last_name . "','" . $this->email . "','" . $this->mobile . "','" . $this->gender . "','" . $this->password . "','" . $this->retype_password . "')";
            //Utility::prx($query);
            $result = mysql_query($query);
            if ($result) {
                $_message = "Your username is " . $this->first_name . " Wait for Admin Approved";
                Utility::message($_message);
                header('Location: user_index.php');
            } else {
                $_message = "There is an error while saving data. Please try again later.";
                Utility::message($_message);
            }
        } else {
            $_message = "Your information is not correct. Please try again.";
            Utility::message($_message);
            header("location: login.php");
        }
    }

    public function login($myusername, $mypassword) {

        $sql = "SELECT * FROM ".$this->table." WHERE first_name='$myusername' and password='$mypassword'";
        $result = mysql_query($sql);
        $count = mysql_num_rows($result);
        if ($count == 1) {
            // Register $myusername, $mypassword and redirect to file "login_success.php"

            $_SESSION['username'] = $myusername;
            $_SESSION['password'] = $mypassword;

            $_message = "Welcome, " . $myusername . "";
            Utility::message($_message);
            header("location: category_index.php");
        } else {
            $_message = "Wrong Username or Password";
            Utility::message($_message);
            header("location: login.php");
        }
    }

    public function index() {

        $objs = array();

        $query = "SELECT * FROM " . $this->table . " ORDER BY user_id DESC";
//        Utility::prx($query);
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $objs[] = $row;
        }
        return $objs;
    }
    
    public function delete($id = NULL) {

        $query = "DELETE FROM " . $this->table . " WHERE `user_id` = '$id'";
//       Utility::prx($query);
        if (mysql_query($query)) {
            $message = "<h2> User is deleted successfully.</h2>";
            Utility::message($message);
            header('Location: user_index.php');
        } else {
            die('Query problem' . mysql_error());
        }
    }
    

}
